var express    = require('express');
var app        = express();
var bodyParser = require('body-parser');
var rotas      = require('./routes');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 3000;

app.use(rotas);

app.use(bodyParser)

app.use(function(req, res, next){
  var data = '';

  req.setEncoding('utf8');

  req.on('data', function(chunk){
    data += chunk;
  });

  console.log(chunk);

  req.on('end', function(){
    req.rawBody = data;
    next();
  });

});

app.listen(port);
console.log('Server rodando na porta ' + port);
