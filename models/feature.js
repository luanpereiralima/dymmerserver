var mongoose = require("../connection/mongo");

var schema = mongoose.Schema;
var ObjectId = schema.ObjectId;

var featureSchema = schema({
  nameXml: {type:String, required:true},
  xml: {type:String, required:true},
  numberOfFeatures:Number,
  numberOfOptionalFeatures:Number,
  numberOfMandatoryFeatures:Number,
	numberOfTopFeatures:Number,
	numberOfLeafFeatures:Number,
  depthOfTreeMax:Number,
	depthOfTreeMedian:Number,
	cognitiveComplexityOfAFeatureModel:Number,
	flexibilityOfConfiguration:Number,
	singleCyclicDependentFeatures:Number,
	multipleCyclicDependentFeatures:Number,
	featureExtendibility:Number,
	cyclomaticComplexity:Number,
	variableCrossTreeConstras:Number,
	compoundComplexity:Number,
	numberOfGroupingFeatures:Number,
	crossTreeConstrasRate:Number,
	coeficientOfConnectivityDensity:Number,
	numberOfVariableFeatures:Number,
	singleVariationPosFeatures:Number,
	multipleVariationPosFeatures:Number,
	rigidNoVariationPosFeatures:Number,
	numberOfValidConfigurations:Number,
	branchingFactorsMax:Number,
	branchingFactorsMedian:Number,
	orRate:Number,
	xorRate:Number,
	ratioOfVariability:Number,
	nonFunctionalCommonality:Number,
  numberOfContexts:Number,
  measuresContexts:[
    {
      nameContext: {type:String, required:true},
      numberOfFeatures:Number,
      numberOfOptionalFeatures:Number,
      numberOfMandatoryFeatures:Number,
      numberOfTopFeatures:Number,
      numberOfLeafFeatures:Number,
      depthOfTreeMax:Number,
      depthOfTreeMedian:Number,
      cognitiveComplexityOfAFeatureModel:Number,
      flexibilityOfConfiguration:Number,
      singleCyclicDependentFeatures:Number,
      multipleCyclicDependentFeatures:Number,
      featureExtendibility:Number,
      cyclomaticComplexity:Number,
      variableCrossTreeConstras:Number,
      compoundComplexity:Number,
      numberOfGroupingFeatures:Number,
      crossTreeConstrasRate:Number,
      coeficientOfConnectivityDensity:Number,
      numberOfVariableFeatures:Number,
      singleVariationPosFeatures:Number,
      multipleVariationPosFeatures:Number,
      rigidNoVariationPosFeatures:Number,
      numberOfValidConfigurations:Number,
      branchingFactorsMax:Number,
      branchingFactorsMedian:Number,
      orRate:Number,
      xorRate:Number,
      ratioOfVariability:Number,
      nonFunctionalCommonality:Number,
      numberOfActivatedFeatures:Number,
      numberOfDeactivatedFeatures:Number,
      numberOfContextConstraints:Number,
      activatedFeaturesByContextAdaptation:Number,
      desactivatedFeaturesByContextAdaptation:Number,
      nonContextFeatures:Number,
      contextFeaturesContraints: Number
    }
  ]
}, { versionKey: false });

featureSchema.statics.buscar = function(retorno){
    this.find({})
    .exec(retorno);
};

module.exports = mongoose.model('Feature', featureSchema);
