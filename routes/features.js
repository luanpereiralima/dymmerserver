var Feature = require('../models/feature');
var express = require('express');
var router = express.Router();

/**
 * @api {get} URL_SERVER/features/ Obter Modelos
 * @apiName Obtem os Modelos de Features
 * @apiGroup Features Model
 *
 */
router.get('/', function(req, res){

  Feature.buscar(
    function(err, posts){

    if(err){
      console.log(err);
      return res.sendStatus(500);
    }

    return res.json(posts);
  })

});

/**
 * @api {post} URL_SERVER/features/ Salvar ou Atualizar Modelos
 * @apiName Salva ou Atualiza
 * @apiGroup Features Model
 *
 * @apiParamExample {json} Request-Example:
 *
  {
  nameXml: {type:String, required:true},
  xml: {type:String, required:true},
  numberOfFeatures:Number,
  numberOfOptionalFeatures:Number,
  numberOfMandatoryFeatures:Number,
 	numberOfTopFeatures:Number,
 	numberOfLeafFeatures:Number,
  depthOfTreeMax:Number,
 	depthOfTreeMedian:Number,
 	cognitiveComplexityOfAFeatureModel:Number,
 	flexibilityOfConfiguration:Number,
 	singleCyclicDependentFeatures:Number,
 	multipleCyclicDependentFeatures:Number,
 	featureExtendibility:Number,
 	cyclomaticComplexity:Number,
 	variableCrossTreeConstras:Number,
 	compoundComplexity:Number,
 	numberOfGroupingFeatures:Number,
 	crossTreeConstrasRate:Number,
 	coeficientOfConnectivityDensity:Number,
 	numberOfVariableFeatures:Number,
 	singleVariationPosFeatures:Number,
 	multipleVariationPosFeatures:Number,
 	rigidNoVariationPosFeatures:Number,
 	numberOfValidConfigurations:Number,
 	branchingFactorsMax:Number,
 	branchingFactorsMedian:Number,
 	orRate:Number,
 	xorRate:Number,
 	ratioOfVariability:Number,
 	nonFunctionalCommonality:Number,
   numberOfContexts:Number,
   medidasContexto:[
     {
       nameContext: {type:String, required:true},
       numberOfFeatures:Number,
       numberOfOptionalFeatures:Number,
       numberOfMandatoryFeatures:Number,
       numberOfTopFeatures:Number,
       numberOfLeafFeatures:Number,
       depthOfTreeMax:Number,
       depthOfTreeMedian:Number,
       cognitiveComplexityOfAFeatureModel:Number,
       flexibilityOfConfiguration:Number,
       singleCyclicDependentFeatures:Number,
       multipleCyclicDependentFeatures:Number,
       featureExtendibility:Number,
       cyclomaticComplexity:Number,
       variableCrossTreeConstras:Number,
       compoundComplexity:Number,
       numberOfGroupingFeatures:Number,
       crossTreeConstrasRate:Number,
       coeficientOfConnectivityDensity:Number,
       numberOfVariableFeatures:Number,
       singleVariationPosFeatures:Number,
       multipleVariationPosFeatures:Number,
       rigidNoVariationPosFeatures:Number,
       numberOfValidConfigurations:Number,
       branchingFactorsMax:Number,
       branchingFactorsMedian:Number,
       orRate:Number,
       xorRate:Number,
       ratioOfVariability:Number,
       nonFunctionalCommonality:Number,
       numberOfActivatedFeatures:Number,
       numberOfDeactivatedFeatures:Number,
       numberOfContextConstraints:Number,
       activatedFeaturesByContextAdaptation:Number,
       desactivatedFeaturesByContextAdaptation:Number,
       nonContextFeatures:Number,
     }
   ]
 }
 * @apiParam {JSON} scheme Schema do JSON, se o schema tiver o id do modelo ele sefrerá o update automaticamente \n o seguinte Schema é necessário para a criação
 *
 */
router.post('/', function(req, res){
  var scheme = req.body.scheme;

  if(!scheme)
    return res.sendStatus(400);

  var json = JSON.parse(scheme);

  var feature = new Feature(json);

  if(json._id)
    feature.isNew = false;

  feature.save(function(err){

    if(err){
      console.log(err);
      return res.sendStatus(500);
    }
    return res.sendStatus(200);
  });

});

/**
 * @api {delete} URL_SERVER/features/ Deletar Modelos
 * @apiName Deleta os Modelos de Features
 * @apiGroup Features Model
 *
 * @apiParam {String} id id do Modelo de Features.
 *
 */

router.delete('/', function(req, res){
  var id = req.body.id;

  if(!id)
    return res.sendStatus(400);

  var feature = new Feature({_id:id});

  feature.remove(function(err){

    if(err){
      console.log(err);
      return res.sendStatus(500);
    }
    return res.sendStatus(200);
  });

});

module.exports = router;
